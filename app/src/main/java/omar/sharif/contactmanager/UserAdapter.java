package omar.sharif.contactmanager;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

public class UserAdapter extends ArrayAdapter<User> {
    private Context context;
    private ArrayList<User> users;

    public UserAdapter(Context context, ArrayList<User> users) {
        super(context, 0, users);
        this.users = users;
        this.context = context;
    }

    public View getView(int position, @Nullable View covertView, @NonNull ViewGroup parent) {
        View row = covertView;

        if (row == null) {
            row = LayoutInflater.from(context).inflate(R.layout.mylist, parent, false);
        }
        User user = users.get(position);

        if (row != null) {
            TextView textViewName = row.findViewById(R.id.viewName);
            TextView textViewPhone = row.findViewById(R.id.phnNumber);
            TextView textViewAge = row.findViewById(R.id.age);
            textViewName.setText(user.getName());
            textViewPhone.setText(user.getPhone());
            textViewAge.setText(user.getAge());
        }
        return row;
    }
}
