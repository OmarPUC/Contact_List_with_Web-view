package omar.sharif.contactmanager;

public class User {
    private int image;
    private String name;
    private String phone;
    private String age;

    public User(String name, String phone, String age, int image) {
        this.image = image;
        this.name = name;
        this.phone = phone;
        this.age = age;

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }
}
