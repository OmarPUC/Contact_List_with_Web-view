package omar.sharif.contactmanager;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private EditText editFirstName;
    private EditText editLastName;
    private EditText editPhone;
    private EditText editAge;
    private ArrayList<User> users = new ArrayList<>();
    private UserAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        editFirstName = findViewById(R.id.firstName);
        editLastName = findViewById(R.id.lastName);
        editPhone = findViewById(R.id.phnNumber);
        editAge = findViewById(R.id.age);

        adapter = new UserAdapter(this, users);
    }

    public void goToLink(View view) {
        Intent intent = new Intent(this, Main2Activity.class);
        switch (view.getId()) {
            case R.id.newnation:
                intent.putExtra("url", "http://www.thedailynewnation.com");
                startActivity(intent);
                break;
            case R.id.marca:
                intent.putExtra("url", "http://www.marca.com/en");
                startActivity(intent);
                break;
            case R.id.prothomalo:
                intent.putExtra("url", "http://www.prothomalo.com");
                startActivity(intent);
                break;

        }
    }

    public void submit(View view) {
    }
}